﻿using System;
using System.Collections.Generic;
using System.Text;
//using System.Data.OracleClient;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Data;
using System.Data.OracleClient;
using WebApplication3.Models;
using ConnectDB;

namespace Services
{


    public interface IInvoiceAdapterServices
    {

        List<InvoiceIntegrateModels> SearchInvoice(string fromDate, string toDate);
    }


    public class InvoiceAdapterServices : IInvoiceAdapterServices
    {
        public static ConnectionDB conn;
        
        public InvoiceIntegrateModels GetInvoiceById(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<InvoiceIntegrateModels> SearchInvoice(string fromDate, string toDate)
        {
            {
                conn = new ConnectionDB(); ///comment
                string dateQuery = "''";
                if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
                {
                    DateTime dateSearchFrom;
                    DateTime dateSearchTo;
                    try
                    {
                        dateSearchFrom = DateTime.Parse(fromDate).ToLocalTime();
                        dateSearchTo = DateTime.Parse(toDate).ToLocalTime();
                    }
                    catch (Exception)
                    {
                        throw new Exception("Dữ liệu ngày không đúng");
                    }

                    if (dateSearchFrom != null && dateSearchTo != null)
                    {
                        if (DateTime.Compare(dateSearchFrom.Date, dateSearchTo.Date) > 0)
                        {
                            throw new Exception("Tìm kiếm từ ngày phải nhỏ hơn hoặc bằng tìm kiếm tới ngày");
                        }

                        fromDate = "'" + dateSearchFrom.Year + dateSearchFrom.Month + dateSearchFrom.Day + "'";
                        toDate = "'" + dateSearchTo.Year + dateSearchTo.Month + dateSearchTo.Day + "'";
                    }
                    else
                    {
                        throw new Exception("Nhập ngày đúng định dạng");
                    }
                }
                else
                {
                    throw new Exception("Phải điền đầy đủ thông tin");
                }

                List<InvoiceIntegrateModels> invoiceIntegrates = new List<InvoiceIntegrateModels>();
                List<InvoiceModel> invoiceModels = new List<InvoiceModel>();


                //--------------------11
                System.Data.OracleClient.OracleCommand cmd = new OracleCommand();
                cmd.CommandText = "SELECT * FROM WITHINF.VW_IF_SALES_TR_SLIP_VNI_5 WHERE" +
                    " STATEMENT_DATE IN(" + fromDate + "," + toDate + ")";

                //todo lấy dữ liệu của khach hang

                //lay du lieu tu database cho vao InvoiceModel
                cmd.Connection = conn.connection;
                System.Data.OracleClient.OracleDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        InvoiceModel invoiceModel = new InvoiceModel();
                        InvoiceIntegrateModels invoiceIntegrate = new InvoiceIntegrateModels();

                        invoiceModel = InitInvoice(reader);
                        invoiceModels.Add(invoiceModel);
                        SwitchInvocie(invoiceModel, invoiceIntegrate);
                    }
                }
                else
                {
                    throw new Exception("No Data In DataBase");
                }

                conn.Close();
                //--------------44

                //======================5
                //System.Data.OracleClient.OracleConnection conn = new System.Data.OracleClient.OracleConnection();

                ////using connection string attributes to connect to Oracle Database
                //conn.ConnectionString = "Data Source=WITHERP; User Id=WITHINF; Password=WITHINF;Pooling=False";
                ////    "User Id=WITHINF;Password=WITHINF;Data Source=" + "(DESCRIPTION =" +
                ////    "(ADDRESS = (PROTOCOL = TCP)(HOST = 113.160.1.246)(PORT = 1521))" +
                ////    "(CONNECT_DATA =" +
                ////      "(SERVER = DEDICATED)" +
                ////      "(SERVICE_NAME = WITHERP)" +
                ////    ")" +
                ////  ")";
                //conn.Open();
                //Oracle.DataAccess.Client.OracleCommand cmd = new OracleCommand();
                //cmd.Connection = conn;
                //cmd.CommandText = "SELECT * FROM VW_IF_SALES_TR_SLIP WHERE ROWNUM <= 3";
                //cmd.CommandType = CommandType.Text;

                //OracleParameter parameter = new OracleParameter();
                //parameter.Direction = ParameterDirection.Output;
                //parameter.DbType = DbType.RefCursor;
                //parameter.ParameterName = "cursor_";
                //cmd.Parameters.Add(parameter);

                //DataTable tbl = new DataTable();

                //OracleDataAdapter reader = new OracleDataAdapter();
                //reader.SelectCommand = cmd;
                //reader.Fill(tbl);

                //foreach (DataRow row in tbl.Rows)
                //{
                //    string s = "";

                //    foreach (DataColumn column in tbl.Columns)
                //    {
                //        s = s + column.ColumnName + ":" + row[column].ToString() + "|";

                //    }
                //    Console.WriteLine(s);
                //    //MessageBox.Show(tbl.Rows[0][0].ToString());
                //}

                //conn.Dispose();
                //==================================================


                //chuyen tu mang InvoiceModel => InvoiceIntegrateModel 
                if (invoiceModels.Count > 0)
                {
                    var listInvoices = invoiceModels.GroupBy(x => x.INVOICE_NO);

                    foreach (var key in listInvoices)
                    {
                        var keyInvoice = key.ToArray();

                        var invoiceDetails = new List<InvoiceDetailsIntegrateModel>();
                        var invoiceExtraDetails = new List<InvoiceExtraDetailsIntegrateModel>();
                        InvoiceIntegrateModels invoiceIntegrate = new InvoiceIntegrateModels();

                        foreach (var invoice in keyInvoice)
                        {
                            //hoa don chinh
                            if (invoice.DR_CR_TYPE_CODE == "DR")
                            {
                                SwitchInvocie(invoice, invoiceIntegrate);
                                invoiceIntegrates.Add(invoiceIntegrate);
                                AddInvoiceExtraDetails(invoiceIntegrate, invoice);
                            }
                            //cac thong tin hoa don chi tiet
                            if (invoice.DR_CR_TYPE_CODE == "CR")
                            {
                                invoiceDetails.Add(GetInvoiceDetails(invoice, invoiceIntegrate));
                            }
                        }
                        invoiceIntegrate.InvoiceDetails = invoiceDetails;
                        CalculateDetails(invoiceIntegrate, invoiceIntegrate.InvoiceDetails.ToList());
                    }
                }

                AddTransaction(invoiceIntegrates);
                return invoiceIntegrates;
            }
        }

        private void AddInvoiceExtraDetails(InvoiceIntegrateModels model, InvoiceModel invoiceModel)
        {
            var invoiceExtras = new List<InvoiceExtraDetailsIntegrateModel>();
            InvoiceExtraDetailsIntegrateModel extra;
            if (!String.IsNullOrEmpty(invoiceModel.LOCKER_NO))
            {
                extra = new InvoiceExtraDetailsIntegrateModel();
                extra.FieldValue = invoiceModel.LOCKER_NO;
                extra.IdInvoice = model.Id;
                extra.IdInvoiceField = 1;
                invoiceExtras.Add(extra);
            }

            if (!String.IsNullOrEmpty(invoiceModel.FOLIO_NO))
            {
                extra = new InvoiceExtraDetailsIntegrateModel();
                extra.FieldValue = invoiceModel.FOLIO_NO;
                extra.IdInvoice = model.Id;
                extra.IdInvoiceField = 2;
                invoiceExtras.Add(extra);
            }

            if (!String.IsNullOrEmpty(invoiceModel.PAYMENT_CODE))
            {
                extra = new InvoiceExtraDetailsIntegrateModel();
                extra.FieldValue = invoiceModel.PAYMENT_CODE;
                extra.IdInvoice = model.Id;
                extra.IdInvoiceField = 3;
                invoiceExtras.Add(extra);
            }

            model.InvoiceExtraDetails = invoiceExtras;
        }

        private InvoiceDetailsIntegrateModel GetInvoiceDetails(InvoiceModel model, InvoiceIntegrateModels invoiceIntegrate)
        {
            var invoiceDetail = new InvoiceDetailsIntegrateModel();
            invoiceDetail.IdInvoice = invoiceIntegrate.Id;

            invoiceDetail.DiscountAmount = model.DISCOUNT;
            invoiceDetail.LastUpdate = model.LAST_UPD_DT;
            invoiceDetail.PaymentAmount = model.TOTAL;
            invoiceDetail.TotalAmount = model.FINAL_AMOUNT + model.TAX;
            invoiceDetail.ProductName = model.OUTLET_NAME;
            invoiceDetail.UnitPrice = model.PRICE;
            invoiceDetail.VatAmount = model.TAX;
            invoiceDetail.Quantity = model.QTY;
            invoiceDetail.ProductId = model.OUTLET_CODE;

            return invoiceDetail;
        }

        private void CalculateDetails(InvoiceIntegrateModels models, List<InvoiceDetailsIntegrateModel> invoiceDetails)
        {
            for (int i = 0; i < invoiceDetails.Count; i++)
            {
                InvoiceDetailsIntegrateModel invoiceDetail = invoiceDetails[i];
                if (invoiceDetail.DiscountAmount != null)
                    models.TotalDiscountAmount += Decimal.Parse(invoiceDetail.DiscountAmount.ToString());

                if (invoiceDetail.TotalAmount != null)
                    models.TotalAmount += Decimal.Parse(invoiceDetail.TotalAmount.ToString());

                if (invoiceDetail.Quantity != null)
                    models.TotalVatAmount += Decimal.Parse(invoiceDetail.VatAmount.ToString());
            }
        }

        ////khoi tao invoiceExtraDetails
        //private void InitInvoiceExtraDetail(OracleDataReader reader, InvoiceIntegrateModels models)
        //{
        //    var invoiceExtras = new List<InvoiceExtraDetailsIntegrateModel>();
        //    InvoiceExtraDetailsIntegrateModel invoiceExtra;

        //    if (!String.IsNullOrEmpty(reader["LOCKER_NO"].ToString()))
        //    {
        //        invoiceExtra = new InvoiceExtraDetailsIntegrateModel();
        //        invoiceExtra.FieldValue = reader["LOCKER_NO"].ToString();
        //        invoiceExtra.IdInvoice = models.Id;
        //        invoiceExtra.IdInvoiceField = 2;
        //        invoiceExtras.Add(invoiceExtra);
        //    }

        //    if (!String.IsNullOrEmpty(reader["FOLIO_NO"].ToString()))
        //    {
        //        invoiceExtra = new InvoiceExtraDetailsIntegrateModel();
        //        invoiceExtra.FieldValue = reader["FOLIO_NO"].ToString();
        //        invoiceExtra.IdInvoice = models.Id;
        //        invoiceExtra.IdInvoiceField = 3;
        //        invoiceExtras.Add(invoiceExtra);
        //    }

        //    if (!String.IsNullOrEmpty(reader["PAYMENT_CODE"].ToString()))
        //    {
        //        invoiceExtra = new InvoiceExtraDetailsIntegrateModel();
        //        invoiceExtra.FieldValue = ConvertPaymentMethodExtra(reader["PAYMENT_CODE"].ToString());
        //        invoiceExtra.IdInvoice = models.Id;
        //        invoiceExtra.IdInvoiceField = 22;
        //        invoiceExtras.Add(invoiceExtra);
        //    }

        //    models.InvoiceExtraDetails = invoiceExtras;
        //}

        //private InvoiceDetailsIntegrateModel InitInvoiceDetail(OracleDataReader reader, InvoiceIntegrateModels models)
        //{
        //    var invoiceDetail = new InvoiceDetailsIntegrateModel();
        //    invoiceDetail.IdInvoice = models.Id;

        //    if (!String.IsNullOrEmpty(reader["DISCOUNT"].ToString()))
        //        invoiceDetail.DiscountAmount = Decimal.Parse(reader["DISCOUNT"].ToString());

        //    invoiceDetail.LastUpdate = models.LastSyncedByTP;


        //    if (!String.IsNullOrEmpty(reader["TOTAL"].ToString()))
        //        invoiceDetail.PaymentAmount = Decimal.Parse(reader["TOTAL"].ToString());

        //    if (!String.IsNullOrEmpty(reader["FINAL_AMOUNT"].ToString()) &&
        //            !String.IsNullOrEmpty(reader["TAX"].ToString()))
        //        invoiceDetail.TotalAmount = Decimal.Parse(reader["FINAL_AMOUNT"].ToString())
        //            + Decimal.Parse(reader["TAX"].ToString());

        //    if (!String.IsNullOrEmpty(reader["OUTLET_NAME"].ToString()))
        //        invoiceDetail.ProductName = reader["OUTLET_NAME"].ToString();

        //    if (!String.IsNullOrEmpty(reader["PRICE"].ToString()))
        //        invoiceDetail.UnitPrice = Decimal.Parse(reader["PRICE"].ToString());

        //    if (!String.IsNullOrEmpty(reader["TAX"].ToString()))
        //        invoiceDetail.VatAmount = Decimal.Parse(reader["TAX"].ToString());

        //    if (!String.IsNullOrEmpty(reader["QTY"].ToString()))
        //        invoiceDetail.Quantity = Double.Parse(reader["QTY"].ToString());

        //    if (!String.IsNullOrEmpty(reader["OUTLET_CODE"].ToString()))
        //        invoiceDetail.ProductId = reader["OUTLET_CODE"].ToString();

        //    return invoiceDetail;

        //}

        private InvoiceModel InitInvoice(OracleDataReader reader)
        {
            var invoice = new InvoiceModel();

            if (!string.IsNullOrEmpty(reader["SERIAL_NO"].ToString())) // 6
            {
                var serial = reader["SERIAL_NO"].ToString();
                byte[] myByteArray = new byte[((string)serial).Length];
                for (int ix = 0; ix < ((string)serial).Length; ++ix)
                {
                    char ch = serial[ix];
                    myByteArray[ix] = (byte)ch;
                }

                serial = Encoding.UTF8.GetString(myByteArray, 0, serial.Length);
                invoice.SERIAL_NO = serial;
            }

            if (!string.IsNullOrEmpty(reader["INVOICE_NO"].ToString())) // 7
                invoice.INVOICE_NO = reader["INVOICE_NO"].ToString();

            if (!string.IsNullOrEmpty(reader["INVOICE_ADJ_FROM"].ToString())) // 9
                invoice.INVOICE_ADJ_FROM = reader["INVOICE_ADJ_FROM"].ToString();

            if (!string.IsNullOrEmpty(reader["FORM_NO"].ToString())) // 10
                invoice.FORM_NO = reader["FORM_NO"].ToString();

            if (!string.IsNullOrEmpty(reader["PAYMENT_CODE"].ToString())) // 19
                invoice.PAYMENT_CODE = reader["PAYMENT_CODE"].ToString();

            if (!string.IsNullOrEmpty(reader["TOTAL"].ToString())) // 21
                invoice.TOTAL = decimal.Parse(reader["TOTAL"].ToString());

            if (!string.IsNullOrEmpty(reader["STATEMENT_DATE"].ToString())) // 24
                invoice.STATEMENT_DATE = ParseDate(reader["STATEMENT_DATE"].ToString());

            if (!string.IsNullOrEmpty(reader["INVOICE_DEL_YN"].ToString()))
                invoice.INVOICE_DEL_YN = reader["INVOICE_DEL_YN"].ToString();

            if (!string.IsNullOrEmpty(reader["LAST_UPD_DT"].ToString())) // 35
                invoice.LAST_UPD_DT = DateTime.Parse(reader["LAST_UPD_DT"].ToString());

            if (!string.IsNullOrEmpty(reader["ADDRESS"].ToString())) // 37
            {
                var address = reader["ADDRESS"];
                invoice.ADDRESS = address.ToString();
                var serial = reader["SERIAL_NO"].ToString();
                byte[] myByteArray = new byte[((string)serial).Length];
                for (int ix = 0; ix < ((string)serial).Length; ++ix)
                {
                    char ch = serial[ix];
                    myByteArray[ix] = (byte)ch;
                }

                serial = Encoding.UTF8.GetString(myByteArray, 0, serial.Length);
                invoice.ADDRESS = reader["ADDRESS"].ToString();
            }

            if (!string.IsNullOrEmpty(reader["FIRST_REG_DT"].ToString())) // 52
                invoice.FIRST_REG_DT = DateTime.Parse(reader["FIRST_REG_DT"].ToString());

            if (!string.IsNullOrEmpty(reader["INVOICE_CUSTOMER_NO"].ToString())) // 55
                invoice.INVOICE_CUSTOMER_NO = (reader["INVOICE_CUSTOMER_NO"].ToString());

            if (!string.IsNullOrEmpty(reader["INVOICE_CUSTOMER_NAME"].ToString())) // 62
                invoice.INVOICE_CUSTOMER_NAME = reader["INVOICE_CUSTOMER_NAME"].ToString();

            if (!string.IsNullOrEmpty(reader["CUSTOMER_TAX_CODE"].ToString())) // 65
                invoice.CUSTOMER_TAX_CODE = reader["CUSTOMER_TAX_CODE"].ToString();

            if (!string.IsNullOrEmpty(reader["MEMB_NO"].ToString())) // 66
                invoice.MEMB_NO = reader["MEMB_NO"].ToString();

            if (!string.IsNullOrEmpty(reader["FIRST_REG_USER_ID"].ToString())) // 66
                invoice.FIRST_REG_USER_ID = reader["FIRST_REG_USER_ID"].ToString();

            if (!string.IsNullOrEmpty(reader["FIRST_REG_USER_IP"].ToString())) // 66
                invoice.FIRST_REG_USER_IP = reader["FIRST_REG_USER_IP"].ToString();

            if (!string.IsNullOrEmpty(reader["LAST_UPD_DT"].ToString())) // 66
                invoice.LAST_UPD_DT = DateTime.Parse(reader["LAST_UPD_DT"].ToString());

            if (!string.IsNullOrEmpty(reader["LAST_UPD_USER_ID"].ToString())) // 66
                invoice.LAST_UPD_USER_ID = reader["LAST_UPD_USER_ID"].ToString();

            if (!string.IsNullOrEmpty(reader["UNIT_CODE"].ToString())) // 66
                invoice.UNIT_CODE = reader["UNIT_CODE"].ToString();

            if (!string.IsNullOrEmpty(reader["MEMB_NO"].ToString())) // 66
                invoice.MEMB_NO = reader["MEMB_NO"].ToString();

            if (!string.IsNullOrEmpty(reader["OUTLET_CODE"].ToString())) // 66
                invoice.OUTLET_CODE = reader["OUTLET_CODE"].ToString();

            if (!string.IsNullOrEmpty(reader["OUTLET_NAME"].ToString())) // 66
                invoice.OUTLET_NAME = reader["OUTLET_NAME"].ToString();

            if (!string.IsNullOrEmpty(reader["PRICE"].ToString())) // 66
                invoice.PRICE = decimal.Parse(reader["PRICE"].ToString());

            if (!string.IsNullOrEmpty(reader["REV_ADJ_REASON"].ToString())) // 66
                invoice.REV_ADJ_REASON = reader["REV_ADJ_REASON"].ToString();

            if (!string.IsNullOrEmpty(reader["REV_ADJ_YN"].ToString())) // 66
                invoice.REV_ADJ_YN = reader["REV_ADJ_YN"].ToString();

            if (!string.IsNullOrEmpty(reader["RE_PRINT_YN"].ToString())) // 66
                invoice.RE_PRINT_YN = reader["RE_PRINT_YN"].ToString();

            if (!string.IsNullOrEmpty(reader["STOCK_CODE"].ToString())) // 66
                invoice.STOCK_CODE = reader["STOCK_CODE"].ToString();

            if (!string.IsNullOrEmpty(reader["VOID_DATE"].ToString())) // 66
                invoice.VOID_DATE = DateTime.Parse(reader["VOID_DATE"].ToString());

            if (!string.IsNullOrEmpty(reader["ITEM_OTH_NAME"].ToString())) // 66
                invoice.ITEM_OTH_NAME = reader["ITEM_OTH_NAME"].ToString();

            //if (!string.IsNullOrEmpty(reader["ITEM_DESCRIPTION"].ToString())) // 66
            //    invoice.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();

            if (!string.IsNullOrEmpty(reader["FOLIO_NO"].ToString())) // 66
                invoice.FOLIO_NO = reader["FOLIO_NO"].ToString();

            if (!string.IsNullOrEmpty(reader["DR_CR_TYPE_CODE"].ToString())) // 66
                invoice.DR_CR_TYPE_CODE = reader["DR_CR_TYPE_CODE"].ToString();

            if (!string.IsNullOrEmpty(reader["DEPOSIT_NO"].ToString())) // 66
                invoice.DEPOSIT_NO = reader["DEPOSIT_NO"].ToString();

            if (!string.IsNullOrEmpty(reader["CUSTOMER_TAX_CODE"].ToString())) // 66
                invoice.CUSTOMER_TAX_CODE = reader["CUSTOMER_TAX_CODE"].ToString();

            //if (!string.IsNullOrEmpty(reader["COMT"].ToString())) // 66
            //    invoice.COMT = reader["COMT"].ToString();

            if (!string.IsNullOrEmpty(reader["COMPANY_NAME"].ToString())) // 66
                invoice.COMPANY_NAME = reader["COMPANY_NAME"].ToString();

            if (!string.IsNullOrEmpty(reader["COMMENTS"].ToString())) // 66
                invoice.COMMENTS = reader["COMMENTS"].ToString();

            if (!string.IsNullOrEmpty(reader["ACCT_DESC"].ToString())) // 66
                invoice.ACCT_DESC = reader["ACCT_DESC"].ToString();

            if (!string.IsNullOrEmpty(reader["LOCKER_NO"].ToString())) // 66
                invoice.LOCKER_NO = reader["LOCKER_NO"].ToString();

            if (!string.IsNullOrEmpty(reader["DISCOUNT"].ToString())) // 66
                invoice.DISCOUNT = Decimal.Parse(reader["DISCOUNT"].ToString());

            if (!string.IsNullOrEmpty(reader["FINAL_AMOUNT"].ToString())) // 66
                invoice.FINAL_AMOUNT = Decimal.Parse(reader["FINAL_AMOUNT"].ToString());

            if (!string.IsNullOrEmpty(reader["QTY"].ToString())) // 66
                invoice.QTY = Double.Parse(reader["QTY"].ToString());

            return invoice;
        }

        private void SwitchInvocie(InvoiceModel model, InvoiceIntegrateModels invoiceIntegrate)
        {
            invoiceIntegrate.Id = Guid.NewGuid();

            invoiceIntegrate.SerialNo = model.SERIAL_NO; //1
            invoiceIntegrate.InvoiceNo = model.INVOICE_NO; //2
            invoiceIntegrate.IdReference = model.INVOICE_ADJ_FROM; //3
            invoiceIntegrate.InvoiceDate = model.STATEMENT_DATE; //4
            invoiceIntegrate.InvoiceStatus = AddInvoiceStatus(model.INVOICE_DEL_YN, model.INVOICE_ADJ_FROM, model.REV_ADJ_YN); //5
            invoiceIntegrate.InvoiceTypeId = model.FORM_NO; //6
            invoiceIntegrate.PaymentMethod = ConvertPaymentMethod(model.PAYMENT_CODE); //7
            invoiceIntegrate.PaymentAmount = model.TOTAL; //8
            invoiceIntegrate.SignStatus = ConverSignStatus(model.INVOICE_DEL_YN, model.INVOICE_ADJ_FROM); //9
            invoiceIntegrate.LastSyncedByTP = model.LAST_UPD_DT; //10
            invoiceIntegrate.SellerAddress = model.ADDRESS; //11
            invoiceIntegrate.BuyerId = model.INVOICE_CUSTOMER_NO; //12
            invoiceIntegrate.BuyerFullName = model.INVOICE_CUSTOMER_NAME; //13
            invoiceIntegrate.BuyerTaxCode = model.CUSTOMER_TAX_CODE; //14
            invoiceIntegrate.BuyerGroup = model.MEMB_NO; //15
            invoiceIntegrate.CreateAt = model.FIRST_REG_DT;


            //    invoice.PaymentAmountWords = reader["PaymentAmountWords"].ToString();
            //    invoice.CreatedBy = reader["CreatedBy"].ToString();
            //    invoice.LastSyncedByVNIs = DateTime.Parse(reader["LastSyncedByVNIs"].ToString());
            //    invoice.PaymentDate = DateTime.Parse(reader["PaymentDate"].ToString());
            //    invoice.IdTransaction = Guid.Parse(reader["IdTransaction".ToUpper()].ToString());
            //    invoice.IdInvoiceTP = reader["IdInvoiceTP".ToUpper()].ToString();
            //    invoice.TemplateNo = reader["TemplateNo".ToUpper()].ToString();
            //    invoice.Code = reader["Code".ToUpper()].ToString();
            //    invoice.Note = reader["Note".ToUpper()].ToString();
            //    invoice.TotalAmount = decimal.Parse(reader["TotalAmount".ToUpper()].ToString());
            //    invoice.TotalVatAmount = decimal.Parse(reader["TotalVatAmount".ToUpper()].ToString());
            //    invoice.TotalDiscountAmount = decimal.Parse(reader["TotalDiscountAmount"].ToString());
            //    invoice.Currency = reader["Currency"].ToString();
            //    invoice.ExchangeRate = double.Parse(reader["ExchangeRate"].ToString());
            //    invoice.BuyerLegalName = reader["BuyerLegalName"].ToString();
            //    invoice.BuyerPhone = reader["BuyerPhone"].ToString();
            //    invoice.BuyerBankName = reader["BuyerBankName"].ToString();
            //    invoice.BuyerCityName = reader["BuyerCityName"].ToString();
            //    invoice.BuyerCountryName = reader["BuyerCountryName"].ToString();
            //    invoice.BuyerDistrictName = reader["BuyerDistrictName"].ToString();
            //    invoice.BuyerEmail = reader["BuyerEmail"].ToString();
            //    invoice.BuyerFax = reader["BuyerFax"].ToString();
            //    invoice.SignedBy = reader["SignedBy"].ToString();
            //    invoice.SignDate = DateTime.Parse(reader["SignDate"].ToString());
            //    invoice.ApprovedBy = reader["ApprovedBy"].ToString();
            //    invoice.ApprovalDate = DateTime.Parse(reader["ApprovalDate"].ToString());
            //    invoice.SignData = reader["SignData"].ToString();
            //    invoice.BuyerAddress = reader["BuyerAddress"].ToString();
            //    invoice.BuyerBankAccount = reader["BuyerBankAccount"].ToString();
            //    invoice.SellerBankAccount = reader["SellerBankAccount"].ToString();
            //    invoice.SellerBankName = reader["SellerBankName"].ToString();
            //    invoice.SellerCountryName = reader["SellerCountryName"].ToString();
            //    invoice.SellerCityName = reader["SellerCityName"].ToString();
            //    invoice.SellerDistrictName = reader["SellerDistrictName"].ToString();
            //    invoice.SellerEmail = reader["SellerEmail"].ToString();
            //    invoice.SellerFax = reader["SellerFax"].ToString();
            //    invoice.SellerFullName = reader["SellerFullName"].ToString();
            //    invoice.SellerFullNameEnglish = reader["SellerFullNameEnglish"].ToString();
            //    invoice.SellerLegalName = reader["SellerLegalName"].ToString();
            //    invoice.SellerPhone = reader["SellerPhone"].ToString();
            //    invoice.SellerTaxCode = reader["SellerTaxCode"].ToString();
            //    invoice.SellerBusinessType = reader["SellerBusinessType"].ToString();
            //    invoice.SellerTaxDepartmentName = reader["SellerTaxDepartmentName"].ToString();
            //    invoice.SyncedByTP = bool.Parse(reader["SyncedByTP"].ToString());
            //    invoice.SyncedByVNIs = bool.Parse(reader["SyncedByVNIs"].ToString());
        }

        public async System.Threading.Tasks.Task GetOrganizationInfoAsync(InvoiceIntegrateModels model)
        {
            //todo: get token => get organization info active from api GET /api/organizations
            //HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://live.vninvoice.vn/");
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(
            //    new MediaTypeWithQualityHeaderValue("application/json"));
            //TokenModel token = new TokenModel();
            //token.UserName = "0101352495";
            //token.Password = "0101352495";

            //string jsonToken = JsonConvert.SerializeObject(token);
            //var httpContent = new StringContent(jsonToken, Encoding.UTF8, "application/json");


            //HttpResponseMessage response = await client.PostAsync("token", httpContent);
            //response.EnsureSuccessStatusCode();
            //string responseBody = await response.Content.ReadAsStringAsync();
            //var tokenObj = JObject.Parse(responseBody);

            //OrganizationInfo organizationInfo = new OrganizationInfo();
            //organizationInfo.

        }

        public DateTime ParseDate(string dateStr)
        {
            string s1 = dateStr[6].ToString() + dateStr[7].ToString();
            string s2 = dateStr[4].ToString() + dateStr[5].ToString();
            string s3 = dateStr.Substring(0, 4);
            string dateFormat = s3 + "-" + s2 + "-" + s1 + "T00:00:00";
            DateTime dt = DateTime.ParseExact(dateFormat, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture).ToLocalTime();
            return dt;
        }

        public string AddInvoiceStatus(string invoiceDel, string invoiceAdjFrom, string revAdj)
        {

            if (invoiceDel.Equals("Y")) //đã xoa bỏ
            {
                if (string.IsNullOrEmpty(invoiceAdjFrom))
                    return "XoaBo";

                if (string.IsNullOrEmpty(revAdj))
                {
                    throw new Exception("Du lieu thieu");
                }
                if (revAdj.Equals("Y"))
                {
                    return "BiThayThe";
                }
                if (revAdj.Equals("N"))
                {
                    return "BiDieuChinh";
                }
                else
                {
                    throw new Exception("Du lieu sai");
                }
            }
            else //invoiceDel = N chua xoa bo
            {
                if (String.IsNullOrEmpty(invoiceAdjFrom))
                {
                    return "Goc";
                }
                if (revAdj.Equals("Y")) //la hoa don goc va bi thay the
                {
                    return "ThayThe";
                }
                if (revAdj.Equals("N"))
                {
                    return "DieuChinh";
                }
                else
                {
                    throw new Exception("Du lieu sai");
                }


            }
        }

        public string ConverSignStatus(string invoiceDel, string invoiceAdjFrom)
        {
            if (invoiceDel.Equals("Y")) //hoa don da bi xoa bo
            {
                return "DaKy";
            }
            else if (invoiceDel.Equals("N") && String.IsNullOrEmpty(invoiceAdjFrom)) //hoa don chua xoa bo, la hoa don goc
            {
                return "ChoDuyet";
            }
            else
            { //hóa đơn chưa xóa bỏ, có giá trị trường thay đổi hóa đơn
                return "Đã ký";
            }
        }


        public void AddFieldIntegrate(InvoiceModel model)
        {
            throw new NotImplementedException();
        }


        public void AddTransaction(List<InvoiceIntegrateModels> invoices)
        {

            for (int i = 0; i < invoices.Count(); i++)
            {
                var invoice1 = invoices.FirstOrDefault(x => x.IdReference == invoices[i].InvoiceNo);
                if (invoice1 == null)
                {
                }
                else
                {
                    if(invoices[i].IdTransaction != null)
                        invoices[i].IdTransaction = Guid.NewGuid();
                    invoice1.IdTransaction = invoices[1].IdTransaction;
                    AddTransaction(invoices);
                }
            }
        }

        public string ConvertPaymentMethod(string method)
        {
            if (String.IsNullOrEmpty(method))
            {
                // todo: chua xu ly
                return null;
            }
            else if (method.Equals("CA"))
            {
                return "TienMat";
            }
            else
            {
                return "TienMatHoacChuyenKhoan";
            }
        }

        public string ConvertPaymentMethodExtra(string methodExtra)
        {
            if (String.IsNullOrEmpty(methodExtra))
            {
                return null;
            }
            else if (methodExtra.Equals("OC") || methodExtra.Equals("ENT"))
            {
                return "Hàng tiêu dùng nội bộ và tiếp khách";
            }
            else
            {
                return null;
            }
        }

        private string GetString(string text)
        {
            string result = "";
            int length = text.Length;
            for (int i = 0; i < length; i++)
            {
                byte[] index;
                index = Encoding.ASCII.GetBytes(text[i].ToString());
                result += Encoding.UTF8.GetString(index);
            }
            return result;
        }
    }


}
