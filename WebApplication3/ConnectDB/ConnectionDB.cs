﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Text;

namespace ConnectDB
{
    public class ConnectionDB : IDisposable
    {
        public const string connect = "Data Source=WITHERP; User Id=WITHINF; Password=WITHINF;Pooling=False";
        public OracleConnection connection;

        public ConnectionDB()
        {
            if ((connection = _connection()) == null)
            {
                this.Dispose();
            }
        }

        private OracleConnection _connection()
        {
            const string connect = "User Id=WITHINF;Password=WITHINF;Data Source=" + "(DESCRIPTION =" +
                    "(ADDRESS = (PROTOCOL = TCP)(HOST = 113.160.1.246)(PORT = 1521))" +
                    "(CONNECT_DATA =" +
                      "(SERVER = DEDICATED)" +
                      "(SERVICE_NAME = WITHERP)" +
                    ")" + ")";
            try
            {
                var connection = new OracleConnection();
                if (String.IsNullOrEmpty(connection.ConnectionString))
                    connection.ConnectionString = connect;
                connection.Open();
                return connection;
            }
            catch
            {
                connection.Dispose();
                return null;
            }
        }

        public void Dispose()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }

        public void Close()
        {
            if (connection != null)
            {
                connection.Close();
            }
        }

    }
}
