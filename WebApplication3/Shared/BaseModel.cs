﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shared.Models
{
    public abstract class BaseModel
    {
    }

    public class BaseModel<T> : BaseModel
    {
        public T Id { get; set; }
    }
}
