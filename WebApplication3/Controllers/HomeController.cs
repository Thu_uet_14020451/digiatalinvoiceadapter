﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication3.Models;
using Services;

namespace WebApplication3.Controllers
{
    public class HomeController : ApiController
    {
        private readonly IInvoiceAdapterServices _invoiceAdapter;

        //public HomeController(IInvoiceAdapterServices invoiceAdapter)
        //{
        //    _invoiceAdapter = invoiceAdapter;
        //}

        [HttpGet]
        public IEnumerable<InvoiceIntegrateModels> GetInvocice(string fromDate, string toDate)
        {
            InvoiceAdapterServices service = new InvoiceAdapterServices();
            return service.SearchInvoice(fromDate, toDate);
        }
    }
}