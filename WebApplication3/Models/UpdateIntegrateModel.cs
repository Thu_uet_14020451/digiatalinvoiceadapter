﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication3.Models
{
    class UpdateIntegrateModel

    {
        public string Id { get; set; }
        public Guid IdInvoice { get; set; }
        public string TemplateNo { get; set; }
        public string InvoiceTypeCode { get; set; }
        public string SerialNo { get; set; }
        public string InvoiceStatus { get; set; }
        public string SignStatus { get; set; }
        public string Code { get; set; }
        public string InvoiceNo { get; set; }
        public bool SyncedByTp { get; set; } // khi dong bo xong set lai la false
        public bool SyncedByVnIs { get; set; }
        public string SyncStatus { get; set; }
        public DateTime LastSyncedByVnIs { get; set; }
    }
}
