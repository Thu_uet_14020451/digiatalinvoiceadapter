﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebApplication3.Models
{
    public class InvoiceModel
    {
        //tuong duong ngay hoa don
        [Required(ErrorMessage = "Ngày hóa đơn không được để trống")]
        public DateTime STATEMENT_DATE { get; set; }

        //Thông tin thanh toán , tổng tiền DR_CR_TYPE_CODE = 'CR' : thông tin chi tiết các item trên hóa đơn
        public string DR_CR_TYPE_CODE { get; set; }

        public string INVOICE_NO { get; set; }

        //ki hieu hoa don
        public string SERIAL_NO { get; set; }

        //InvoiceTypeId 
        public string FORM_NO { get; set; }

        public string LOCKER_NO { get; set; }

        //tuong duong ngay hoa don
        public string FOLIO_NO { get; set; }

        public string MEMB_NO { get; set; }

        public string PAYMENT_CODE { get; set; }

        //tuong duong ngay hoa don
        public string OUTLET_CODE { get; set; }

        public string OUTLET_NAME { get; set; }

        public decimal PRICE { get; set; }

        public string STOCK_CODE { get; set; }

        public string UNIT_CODE { get; set; }

        public string COMPANY_NAME { get; set; }

        public string CUSTOMER_TAX_CODE { get; set; }

        public string ADDRESS { get; set; }

        public string ITEM_CODE { get; set; }

        public string ITEM_DESCRIPTION { get; set; }

        public string ITEM_OTH_NAME { get; set; }

        public string RE_PRINT_YN { get; set; }

        public DateTime FIRST_REG_DT { get; set; }

        public string FIRST_REG_USER_ID { get; set; }

        public string FIRST_REG_USER_IP { get; set; }

        public DateTime LAST_UPD_DT { get; set; }

        public string LAST_UPD_USER_ID { get; set; }

        public string LAST_UPD_USER_IP { get; set; }

        public string DEL_YN { get; set; }

        public string COMMENTS { get; set; }

        public string ACCT_DESC { get; set; }

        public string DEPOSIT_NO { get; set; }

        public string INVOICE_CUSTOMER_NO { get; set; }

        public string INVOICE_CUSTOMER_NAME { get; set; }

        public string COMT { get; set; }

        public string INVOICE_DEL_YN { get; set; }

        public DateTime VOID_DATE { get; set; }

        public string REV_ADJ_YN { get; set; }

        public string INVOICE_ADJ_FROM { get; set; }

        public string REV_ADJ_REASON { get; set; }

        public string INVOICE_STATUS { get; set; }

        public Decimal TOTAL { get; set; }

        public Decimal DISCOUNT { get; set; }

        public Decimal FINAL_AMOUNT { get; set; }

        public Double QTY { get; set; }

        public Decimal TAX { get; set; }



    }
}
