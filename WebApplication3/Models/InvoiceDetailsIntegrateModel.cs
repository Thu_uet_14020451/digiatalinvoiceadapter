﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication3.Models
{
    public class InvoiceDetailsIntegrateModel

    {
        public int Id { get; set; }

        public Guid? IdInvoice { get; set; }

        public decimal? DiscountAmount { get; set; }

        public double? DiscountPercent { get; set; }

        public decimal? PaymentAmount { get; set; }

        public string ProductId { get; set; }

        public string ProductName { get; set; }

        public string UnitName { get; set; }

        public decimal? UnitPrice { get; set; }

        public double? Quantity { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal? VatAmount { get; set; }

        public double? VatPercent { get; set; }

        public string Note { get; set; }

        public DateTime? LastUpdate { get; set; }

    }
}
