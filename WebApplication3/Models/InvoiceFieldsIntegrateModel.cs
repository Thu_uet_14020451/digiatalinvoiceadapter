﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication3.Models
{
    public class InvoiceFieldsIntegrateModel
    {
        public int Id { get; set; }

        public string FieldName { get; set; }

        public string DisplayName { get; set; }

        public string DataType { get; set; }

        public string Metadata { get; set; }

    }
}
