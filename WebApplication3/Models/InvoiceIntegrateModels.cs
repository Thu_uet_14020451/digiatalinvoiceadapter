﻿
using Shared.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebApplication3.Models
{
    public class InvoiceIntegrateModels : BaseModel<Guid>
    {
        [Display(Name = "Id của giao dịch (chỉ có khi sử dụng API tích hợp tạo hóa đơn)")]
        public Guid? IdTransaction { get; set; }

        public string IdInvoiceTP { get; set; }

        public Guid? IdInvoiceVNIs { get; set; }

        public string TaxCode { get; set; }

        [Display(Name = "Mẫu số")]
        public string TemplateNo { get; set; } //01GTKT0/008
        
        public string SerialNo { get; set; }

        public string InvoiceNo { get; set; }

        [Display(Name = "Mã hóa đơn")]
        public string Code { get; set; } //MST|TemplateNo|SerialNo|InvoiceNo

        [Display(Name = "")]
        public string IdReference { get; set; } 
        
        public string InvoiceTypeId { get; set; }

        public string Note { get; set; }
        [Display(Name = "Phần trăm thuế VAT")]
        public double VatPercent { get; set; } //% VAT

        [Display(Name = "Giá trị thuế VAT")]
        public decimal VatAmount { get; set; } //Tien VAT

        [Display(Name = "Tổng tiền trước thuế")]
        public decimal TotalAmount { get; set; } //Tong tien truoc thue

        [Display(Name = "Tổng tiền trước thuế")]
        public decimal TotalVatAmount { get; set; } //Tong tien thue

        [Display(Name = "Phan tram giam gia")]
        public double DiscountPercent { get; set; }

        [Display(Name = "Tong tien giam gia")]
        public decimal TotalDiscountAmount { get; set; }


        [Display(Name = "Chuyen toi loai tien te")]
        public string Currency { get; set; }

        [Display(Name = "ty gia")]
        public double ExchangeRate { get; set; }

        [Display(Name = "Phuong thuc thanh toan")]
        public string PaymentMethod { get; set; }

        [Display(Name = "Ngay thanh toan")]
        public DateTime PaymentDate { get; set; }

        [Display(Name = "So tien thanh toan")]
        public decimal PaymentAmount { get; set; }

        [Display(Name = "So tien thanh toan viet bang chu")]
        public string PaymentAmountWords { get; set; }

        [Display(Name = "Duoc tao boi")]
        public string CreatedBy { get; set; }

        [Display(Name = "Ngay hoa don")]
        public DateTime InvoiceDate { get; set; }

        [Display(Name = "Duoc ki boi")]
        public string SignedBy { get; set; }

        [Display(Name = "ngay ki")]
        public DateTime SignDate { get; set; }

        [Display(Name = "Duyet boi")]
        public string ApprovedBy { get; set; }

        [Display(Name = "ngay duyet")]
        public DateTime? ApprovalDate { get; set; }

        [Display(Name = "Du lieu duoc ki")]
        public string SignData { get; set; }

        [Display(Name = "trang thai hoa don")]
        public string InvoiceStatus { get; set; }

        [Display(Name = "trang thai ki")]
        public string SignStatus { get; set; }

        [Display(Name = "Dong bo boi ERP hay khong")]
        public bool SyncedByTP { get; set; }

        [Display(Name = "Dong bo boi VNIs hay khong")]
        public bool SyncedByVNIs { get; set; }

        [Display(Name = "lan cuoi dong bo boi ERP")]
        public DateTime LastSyncedByTP { get; set; }

        [Display(Name = "lan cuoi dong bo boi Vnis")]
        public DateTime LastSyncedByVNIs { get; set; }

        [Display(Name = "Dia chi cong ty")]
        public string SellerAddress { get; set; }

        [Display(Name = "So tai khoan ngan hang cong ty")]
        public string SellerBankAccount { get; set; }

        [Display(Name = "Ten tai khoan ngan hang cua cong ty")]
        public string SellerBankName { get; set; }

        [Display(Name = "Ten quoc gia cua cong ty")]
        public string SellerCountryName { get; set; }

        [Display(Name = "ten thanh pho")]
        public string SellerCityName { get; set; }

        [Display(Name = "Ten quan cua cong ty")]
        public string SellerDistrictName { get; set; }

        [Display(Name = "email cong ty")]
        public string SellerEmail { get; set; }

        [Display(Name = "so fax cong ty")]
        public string SellerFax { get; set; }

        [Display(Name = "ten cong ty")]
        public string SellerFullName { get; set; }

        [Display(Name = "ten cong ty tieng anh")]
        public string SellerFullNameEnglish { get; set; }

        [Display(Name = "ten phap nhan")]
        public string SellerLegalName { get; set; }

        [Display(Name = "so dien thoai cong ty")]
        public string SellerPhone { get; set; }

        [Display(Name = "ma so thue cong ty")]
        public string SellerTaxCode { get; set; }

        [Display(Name = "Loaji hinh kinh doanh")]
        public string SellerBusinessType { get; set; }

        [Display(Name = "co quan thue")]
        public string SellerTaxDepartmentName { get; set; }

        [Display(Name = "Tao vao ngay")]
        public DateTime CreateAt { get; set; }

        public string BuyerId { get; set; }

        [Display(Name = "Dia chi khach hang")]
        public string BuyerAddress { get; set; }

        [Display(Name = "So tai khoan ngan hang khach hang")]
        public string BuyerBankAccount { get; set; }

        [Display(Name = "Ten tai khoan ngan hang cua khach hang")]
        public string BuyerBankName { get; set; }

        [Display(Name = "Thanh pho dia chi khach hang")]
        public string BuyerCityName { get; set; }

        [Display(Name = "quoc gia dia chi khach hang")]
        public string BuyerCountryName { get; set; }

        [Display(Name = "quan dia chi khach hang")]
        public string BuyerDistrictName { get; set; }

        [Display(Name = "email khach hang")]
        public string BuyerEmail { get; set; }

        [Display(Name = "so fax khach hang")]
        public string BuyerFax { get; set; }

        [Display(Name = "ten day du khach hang")]
        public string BuyerFullName { get; set; }

        [Display(Name = "ten phap nhan khach hang")]
        public string BuyerLegalName { get; set; }

        [Display(Name = "so dien thoai khach hang")]
        public string BuyerPhone { get; set; }

        [Display(Name = "Ma so thue khach hang")]
        public string BuyerTaxCode { get; set; }

        [Display(Name = "Nhom khach hang")]
        public string BuyerGroup { get; set; }
        public IEnumerable<InvoiceDetailsIntegrateModel> InvoiceDetails { get; set; }

        public IEnumerable<InvoiceExtraDetailsIntegrateModel> InvoiceExtraDetails { get; set; }

    }
}
