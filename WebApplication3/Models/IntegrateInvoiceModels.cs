﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication3.Models
{
    public class IntegrateInvoiceModels
    {

        public int TotalRow { get; set; }

        public Guid Id { get; set; }

        public string IdIntegrate { get; set; }

        public Guid? IdTransaction { get; set; }

        public string IdInvoiceTp { get; set; }

        public string IdInvoiceVnIs { get; set; }

        public string TemplateNo { get; set; }

        public string SerialNo { get; set; }

        public string InvoiceNo { get; set; }

        public string Code { get; set; }

        public string IdReference { get; set; }

        public string InvoiceTypeId { get; set; }

        public string Note { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal? TotalVatAmount { get; set; }

        public decimal? TotalDiscountAmount { get; set; }

        public string Currency { get; set; }

        public double? ExchangeRate { get; set; }

        public string PaymentMethod { get; set; }

        public DateTime? PaymentDate { get; set; }

        public decimal? PaymentAmount { get; set; }

        public string PaymentAmountWords { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string SignedBy { get; set; }

        public DateTime? SignDate { get; set; }

        public string ApprovedBy { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public string SignData { get; set; }

        public string InvoiceStatus { get; set; }

        public string SignStatus { get; set; }

        public bool? SyncedByTp { get; set; }

        public bool? SyncedByVnIs { get; set; }

        public DateTime? LastSyncedByTp { get; set; }

        public DateTime? LastSyncedByVnIs { get; set; }

        public string SellerAddress { get; set; }

        public string SellerBankAccount { get; set; }

        public string SellerBankName { get; set; }

        public string SellerCountryName { get; set; }

        public string SellerCityName { get; set; }

        public string SellerDistrictName { get; set; }

        public string SellerEmail { get; set; }

        public string SellerFax { get; set; }

        public string SellerFullName { get; set; }

        public string SellerFullNameEnglish { get; set; }

        public string SellerLegalName { get; set; }

        public string SellerPhone { get; set; }


        public string SellerTaxCode { get; set; }

        public string SellerBusinessType { get; set; }

        public string SellerTaxDepartmentName { get; set; }

        public DateTime? CreateAt { get; set; }

        public string BuyerAddress { get; set; }

        public string BuyerBankAccount { get; set; }

        public string BuyerId { get; set; }

        public string BuyerBankName { get; set; }

        public string BuyerCityName { get; set; }

        public string BuyerCountryName { get; set; }

        public string BuyerDistrictName { get; set; }

        public string BuyerEmail { get; set; }

        public string BuyerFax { get; set; }

        public string BuyerFullName { get; set; }

        public string BuyerLegalName { get; set; }

        public string BuyerPhone { get; set; }

        public string BuyerTaxCode { get; set; }
        public string BuyerGroup { get; set; }

        public IEnumerable<InvoiceDetailsIntegrateModel> InvoiceDetails { get; set; }

        public IEnumerable<InvoiceExtraDetailsIntegrateModel> InvoiceExtraDetails { get; set; }

    }
}
