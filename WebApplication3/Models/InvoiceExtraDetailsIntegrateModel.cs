﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication3.Models
{
    public class InvoiceExtraDetailsIntegrateModel

    {
        public int Id { get; set; }

        public Guid? IdInvoice { get; set; }

        public int? IdInvoiceField { get; set; }

        public string FieldValue { get; set; }
       
    }
}
