﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication3.Models
{
    public class IntegrateExcelModel
    {
        public string INVOICE_DATE { get; set; }

        public string INVOICE_NO { get; set; }

        public string FOLIO_NO { get; set; }

        public string LOCKER_NO { get; set; }

        public string MEMB_NO { get; set; }

        public string CHCK_IN_PRSN_NAME { get; set; }

        public string OUTLET_CODE { get; set; }

        public string OUTLET_NAME { get; set; }

        public string ITEM_CODE { get; set; }

        public string ITEM_OTH_NAME { get; set; }

        public string REV_QTY { get; set; }

        public string PRICE { get; set; }

        public string AMOUNT { get; set; }

        public string DC_AMT { get; set; }

        public string FINAL_AMOUNT { get; set; }

        public string VAT { get; set; }

        public string TOTAL { get; set; }

        public string PAYMENT { get; set; }

        public string COMPANY_NAME { get; set; }

        public string TAX_CODE { get; set; }

        public string ADDRESS { get; set; }

    }

}
