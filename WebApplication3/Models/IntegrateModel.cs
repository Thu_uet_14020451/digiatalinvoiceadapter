﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebApplication3.Models
{
    public class IntegrateModel
    {
        [Required(ErrorMessage = "Vui lòng chọn hóa đơn")]
        public List<Guid> Ids { get; set; }

        public bool IsCombine { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn mẫu hóa đơn đã đăng ký phát hành")]
        public int IdRegisterTemplateInvoice { get; set; }

        public int IdInvoiceType { get; set; }

        public int IdInvoiceTemplate { get; set; }

        public Guid? CreateBy { get; set; }

        public Guid? OrganizationCode { get; set; }

        public List<Guid> Owners { get; set; }
    }

}
